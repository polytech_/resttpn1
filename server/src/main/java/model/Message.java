package model;

import javax.persistence.*;

@Entity
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "poster_id")
    private User messagePoster;

    private String messageText;

    public Message(String messageText, User messagePoster) {
        this.messageText = messageText;
        this.messagePoster = messagePoster;
    }

    public Message() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getMessagePoster() {
        return messagePoster;
    }

    public void setMessagePoster(User messagePoster) {
        this.messagePoster = messagePoster;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

}

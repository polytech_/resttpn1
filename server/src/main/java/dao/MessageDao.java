package dao;

import model.Message;
import model.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.engine.transaction.internal.TransactionImpl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class MessageDao {

    private final Session hibernateSession;

    public MessageDao(Session hibernateSession) {
        this.hibernateSession = hibernateSession;
    }

    public Optional<Message> getMessage(int idMessage) {
        try{
            return Optional.of(hibernateSession.find(Message.class, idMessage));
        }catch (Exception e){
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public List<Message> getMessagesOfUser(String posterUsername) {
        try{
            return hibernateSession.createQuery("select message from Message message where message.messagePoster.username =: username", Message.class)
                    .setParameter("username", posterUsername)
                    .getResultList();
        }catch (Exception e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public Message addMessage(Message message) {
        Transaction transaction = hibernateSession.beginTransaction();
        hibernateSession.save(message);
        transaction.commit();
        return message;
    }

    public Message updateMessage(int idMessage, Message newData) {

        if(!getMessage(idMessage).isPresent()) {
            return null;
        }

        Message messageToUpdate = getMessage(idMessage).get();

        if(messageToUpdate.getMessageText().equals(newData.getMessageText())) {
            return null;
        }

        messageToUpdate.setMessageText(newData.getMessageText());

        Transaction transaction = hibernateSession.getTransaction();
        transaction.begin();
        Message updatedMessage = (Message)hibernateSession.merge(messageToUpdate);
        transaction.commit();

        return updatedMessage;
    }

    public void deleteMessage(int idMessage) {
        Transaction transaction = hibernateSession.getTransaction();
        transaction.begin();
        if(!getMessage(idMessage).isPresent()) {
            return;
        }
        Message messageToDelete = getMessage(idMessage).get();
        hibernateSession.remove(messageToDelete);
        transaction.commit();
    }

    public List<Message> getAllMessages() {
        try{
            return hibernateSession.createQuery("select message from Message message", Message.class)
                    .getResultList();
        }catch (Exception e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
}

package dao;
import model.User;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class UserDao {

    private final Session hibernateSession;

    public UserDao(Session hibernateSession) {
        this.hibernateSession = hibernateSession;
    }

    public User getUserByCredentials(String username, String password) {

        Optional<User> user = hibernateSession.createQuery("SELECT user FROM User user WHERE user.username =:username AND user.password =:password", User.class)
                .setParameter("username", username)
                .setParameter("password", password)
                .getResultList()
                .stream()
                .findFirst();

        return user.get();
    }

    public Optional<User> getUserByUsername(String username){
        try{
            return hibernateSession.createQuery("SELECT user FROM User user WHERE user.username =:username", User.class)
                    .setParameter("username", username)
                    .getResultList()
                    .stream()
                    .findFirst();
        }catch (Exception e){
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public void addUser(User user) {
        Transaction transaction = hibernateSession.beginTransaction();
        hibernateSession.save(user);
        transaction.commit();
    }

    public List<User> getAllUsers() {
        try{
            return hibernateSession.createQuery("select user from User user", User.class)
                    .getResultList();
        }catch (Exception e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
}

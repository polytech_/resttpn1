package initializeDatabase;

import dao.MessageDao;
import dao.UserDao;
import model.Message;
import model.User;
import org.hibernate.Session;
import utils.HibernateUtil;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionListener;
import java.util.List;
import java.util.Optional;


@WebListener()
public class databaseInitializer implements ServletContextListener,
        HttpSessionListener, HttpSessionAttributeListener {

    // Public constructor is required by servlet spec
    public databaseInitializer() {
    }

    //Méthode d'initialisation de la base de données (class instanciée lors du déploiement)
    public void contextInitialized(ServletContextEvent sce) {

        Session hibernateSession = HibernateUtil.getSession();

        UserDao userDao = new UserDao(hibernateSession);
        MessageDao messageDao = new MessageDao(hibernateSession);
        
        if(userDao.getAllUsers().isEmpty()) {
            userDao.addUser(new User("othmane97", "mdp1", "Othmane Allamou"));
            userDao.addUser(new User("nour97", "mdp2", "Nour Nasrallah"));
            userDao.addUser(new User("yang97", "mdp3", "Yang Yang"));
            userDao.addUser(new User("tariq96", "mdp4", "Tariq Chaairat"));
        }
        
        if(messageDao.getAllMessages().isEmpty()) {

            Message comment1 = new Message("laudantium enim quasi est quidem magnam voluptate", userDao.getUserByUsername("othmane97").get());
            Message comment2 = new Message("est natus enim nihil est dolore omnis voluptatem numquam ",userDao.getUserByUsername("nour97").get());
            Message comment3 = new Message("harum non quasi et ratione ntempore iure ex voluptates ", userDao.getUserByUsername("yang97").get());
            Message comment4 = new Message("ut voluptatem corrupti velit nad voluptatem maiores net", userDao.getUserByUsername("tariq96").get());
            Message comment5 = new Message("expedita maiores dignissimos facilis nipsum est rem est fugit", userDao.getUserByUsername("othmane97").get());
            Message comment6 = new Message("nihil ut voluptates blanditiis autem odio dicta rerum nquisquam", userDao.getUserByUsername("nour97").get());
            Message comment7 = new Message("veritatis voluptates necessitatibus maiores corrupti neque", userDao.getUserByUsername("yang97").get());
            Message comment8 = new Message("deleniti aut sed molestias explicabo ncommodi odio ratione", userDao.getUserByUsername("tariq96").get());

            messageDao.addMessage(comment1);
            messageDao.addMessage(comment2);
            messageDao.addMessage(comment3);
            messageDao.addMessage(comment4);
            messageDao.addMessage(comment5);
            messageDao.addMessage(comment6);
            messageDao.addMessage(comment7);
            messageDao.addMessage(comment8);
        }
    }
}

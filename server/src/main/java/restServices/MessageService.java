package restServices;

import dao.MessageDao;
import model.Message;
import utils.HibernateUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

// The Java class will be hosted at the URI path "/MessageService"
@Path("/MessagesService")
public class MessageService {

    private MessageDao messageDao = new MessageDao(HibernateUtil.getSession());

    /**
     * Méthode du service retournant la liste de tous les messages présents dans la bdd
     * @return la liste des messages
     */
    @GET
    // The Java method will produce content identified by the MIME Media type "APPLICATION_JSON"
    @Path("/messages")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Message> getAllMessages() {
        List<Message> messages = messageDao.getAllMessages();
        return messages;
    }


    /**
     * Méthode du service retournant les messages postés par l'utilisateur correspondant pseudo en paramètre
     * @param username le nom d'utilisateur
     * @return la liste des messages postés par l'utilisateur
     */
    @GET
    @Path("/{username}/messages")
    // The Java method will produce content identified by the MIME Media type "APPLICATION_JSON"
    @Produces(MediaType.APPLICATION_JSON)
    public List<Message> getMessagesOfUser(@PathParam("username") String username) {
        return messageDao.getMessagesOfUser(username);
    }


    /**
     * Retourne un message à partir de son id
     * @param idMessage L'id du message à retourner
     * @return le message
     */
    @GET
    // The Java method will produce content identified by the MIME Media type "APPLICATION_JSON"
    @Path("/messages/{idMessage}")
    @Produces(MediaType.APPLICATION_JSON)
    public Message getMessage(@PathParam("idMessage") int idMessage) {
        return messageDao.getMessage(idMessage).get();
    }


    /**
     * Méthode du service servant à ajouter un message dans la base de données
     * @param message l'objet message à ajouter
     * @return le message ajouté
     */
    @POST
    //Obliges le client à fournir des données de type JSON
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/messages")
    public Message addMessage(Message message) {
        return message.getMessageText().isEmpty() ? null : messageDao.addMessage(message);
    }


    /**
     * Méthode du service servant à modifier un message existant
     * @param idMessage l'id du message existant
     * @param newData le message contenant les nouvelles données
     * @return le message modifié
     */
    @PUT
    //Obliges le client à fournir des données de type JSON
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/update/{idMessage}")
    public Message updateMessage(@PathParam("idMessage") int idMessage, Message newData) {
        //On set l'id du message qui contient les nouvelles données pour être sur de get le bon message de la bdd
        newData.setId(idMessage);
        return messageDao.updateMessage(idMessage, newData);
    }


    /**
     * Méthode du service servant à supprimer un message de la base de données
     * @param idMessage l'id du message à supprimer
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/delete/{idMessage}")
    public void deleteMessage(@PathParam("idMessage") int idMessage) {
       messageDao.deleteMessage(idMessage);
    }

}

package restServices;

import dao.UserDao;
import model.User;
import utils.HibernateUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

// Les services concernant les utilisateurs seront accessibles à partir de /UserService
@Path("/UserService")
public class UserService {
    private UserDao userDao = new UserDao(HibernateUtil.getSession());

    /**
     * Service REST retournant tous les utilisateurs présents dans la base de données
     * @return La liste des utilisateurs
     */
    @GET
    @Path("/users")
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> allUsers(){
        return userDao.getAllUsers();
    }


    /**
     * Service REST retournant un utilisateur à partir de ses identifiants
     * @param username le nom d'utilisateur
     * @param password le mot de passe
     * @return
     */
    @GET
    @Path("/user")
    @Produces(MediaType.APPLICATION_JSON)
    public User getUserByCredentials(@QueryParam("username") String username,
                                     @QueryParam("password") String password) {

        return userDao.getUserByCredentials(username, password);
    }

}

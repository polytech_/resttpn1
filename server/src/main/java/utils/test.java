package utils;


import dao.UserDao;
import model.User;

/**
 * Classe main de test des services REST et des dao
 */
public class test {

    public static void main(String[] args) {

        UserDao userDao = new UserDao(HibernateUtil.getSession());

        User user = userDao.getUserByCredentials("othmane97", "mdp1");

        System.out.println(user);
    }
}

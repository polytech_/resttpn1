<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Mes messages</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="resources/css/style.css"/>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand">Bienvenue au blog ${sessionScope.ids.split("/")[0]} !</a>

    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto">
            <div class="nav-item active">
                <a class="nav-link" href="messages">Retour au blog</a>
            </div>
        </ul>
        <div class="nav-item" style="margin-right: 10px">
            <a class="btn btn-success" href="addMessage.jsp">Poster</a>
        </div>
        <div class="nav-item">
            <a class="btn btn-danger" href="logout">Se déconnecter</a>
        </div>
    </div>
</nav>


<div class="container-fluid" style="width: 70%">
    <br>
    <c:if test="${requestScope.errorModify!=null}">
        <p class="error">${requestScope.errorModify}</p>
    </c:if>
    <c:if test="${requestScope.confirmModify!=null}">
        <p class="confirmation">${requestScope.confirmModify}</p>
    </c:if>
    <c:if test="${requestScope.confirmDelete!=null}">
        <p class="confirmation">${requestScope.confirmDelete}</p>
    </c:if>
    <c:if test="${requestScope.confirmPost!=null}">
        <p class="confirmation">${requestScope.confirmPost}</p>
    </c:if>
    <h3>Mes messages :</h3>
    <br>
    <c:forEach items="${myMessages}" var="myMessage">
        <p>Utilisateur : <b>${myMessage.messagePoster.fullName}</b></p>
        <p class="message">${myMessage.messageText}</p>
        <a href="messages?action=modify&messageId=${myMessage.id}" class="btn btn-secondary">Modifier</a>
        <a href="messages?action=delete&messageId=${myMessage.id}" class="btn btn-danger">Supprimer</a>
        <br>
        <br>
    </c:forEach>
</div>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>

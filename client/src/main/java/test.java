
import model.Message;
import model.User;
import restClient.MessageClient;
import restClient.UserClient;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Classe main dans laquelle on test notre client avant de l'intégrer à l'application
 */
public class test {

    public static void main(String[] args) {

        ArrayList<User> users = UserClient.getAllUsers();
        //ArrayList<Message> msgs = new ArrayList<>(MessageClient.getAllMessages());

        Message msg = MessageClient.getMessage(1);
        User user = UserClient.getUserByCredentials("othmane97", "mdp1");

        System.out.println(msg);

    }
}

package filter;

import restClient.UserClient;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter( filterName = "SignInFilter",
            urlPatterns = "/messages")

public class SignInFilter implements Filter {

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        if(request.getSession(false)==null || request.getSession(false).getAttribute("ids")==null) {
            response.sendRedirect("login.jsp");
        }
        else {
            String ids = (String)request.getSession(false).getAttribute("ids");

            String username = ids.split("/")[0];
            String password = ids.split("/")[1];

            //Si la session contient les mauvais identifiants

            if (UserClient.getUserByCredentials(username, password)==null) {
                //On la supprime
                request.getSession(false).invalidate();

                //Renvoi de l'erreur
                request.setAttribute("error", "Identifiants incorrects !");
                request.getRequestDispatcher("login.jsp").forward(request, response);

            } else {
                chain.doFilter(request, response);
            }
        }
    }

}

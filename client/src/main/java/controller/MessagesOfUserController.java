package controller;

import model.Message;
import model.User;
import restClient.MessageClient;
import restClient.UserClient;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "MessagesOfUserController",
            urlPatterns = "/messagesOfUser")
public class MessagesOfUserController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String username = request.getParameter("username");

        //Si l'utilisateur de l'application n'a sélectionné aucun utilisateur dans le select,
        // on revoie une erreur à la page des messages
        if(username.equals("-1")) {
            request.setAttribute("error", "Merci de sélectionner le nom d'un utilisateur.");
            request.getRequestDispatcher("messages").forward(request, response);
            return;
        }

        //On utilise le client des messages qui va appeler le service des messages afin de retourner
        //la liste de tous les messages présents dans la bdd
        List<Message> messages = MessageClient.getMessagesOfUser(username);

        request.setAttribute("messages", new ArrayList<>(messages));

        //On transmets la liste à la page d'affichage
        request.getRequestDispatcher("viewMessage.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

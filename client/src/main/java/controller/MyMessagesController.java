package controller;

import model.Message;
import restClient.MessageClient;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "MyMessagesController",
            urlPatterns = "/mymessages")
public class MyMessagesController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession(false);

        //Récupération de l'utilisateur connecté :
        String ids = (String)session.getAttribute("ids");

        String username = ids.split("/")[0];

        ArrayList<Message> myMessages = new ArrayList<>(MessageClient.getMessagesOfUser(username));

        //On mets la listes des messages de l'utilisateur connectés dans la requête
        request.setAttribute("myMessages", myMessages);

        //On envoie la requête à la page des messages afin que les messages y soient affichés
        request.getRequestDispatcher("myMessages.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}

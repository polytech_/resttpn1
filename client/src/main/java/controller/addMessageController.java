package controller;

import model.Message;
import model.User;
import restClient.MessageClient;
import restClient.UserClient;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "addMessageController",
            urlPatterns = "/addMessage")
public class addMessageController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //Récupération des données relatives au message
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String messageText = request.getParameter("messageText");

        //Si le message ne contient aucun texte, on renvoie une erreur à la page d'ajout
        if(messageText.isEmpty()) {
            request.setAttribute("error", "Le contenu du message est vide, merci d'en rajouter.");
            request.getRequestDispatcher("addMessage.jsp").forward(request, response);
            return;
        }

        //Récupération de l'objet utilisateur du posteur du message
        User poster = UserClient.getUserByCredentials(username, password);

        //Création de l'objet message à partir des données précédentes
        Message messageToAdd = new Message(messageText, poster);

        //Ajout du message à partir du client consommant le service REST des messages
        MessageClient.addMessage(messageToAdd);

        //Transmission des données au service REST afin d'effectuer la mise à jour dans la bdd
        request.setAttribute("confirmPost", "Le message a bien été posté !");

        request.getRequestDispatcher("mymessages").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

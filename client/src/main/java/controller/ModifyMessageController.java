package controller;

import model.Message;
import model.User;
import restClient.MessageClient;
import restClient.UserClient;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ModifyMerssageController",
            urlPatterns = "/modify")
public class ModifyMessageController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //Récupération des données relatives au message
        int messageId = Integer.parseInt(request.getParameter("messageId"));
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String messageText = request.getParameter("messageText");

        //Récupération de l'objet utilisateur du posteur du message
        User poster = UserClient.getUserByCredentials(username, password);

        //Récupération de l'objet message à partir des données précédentes
        Message newData = new Message(messageText, poster);

        //Transmission des données au service REST afin d'effectuer la mise à jour dans la bdd
        Message updatedMessage = MessageClient.updateMessage(messageId, newData);

        //Si le texte du message n'a pas été changé
        if(updatedMessage==null) {
            request.setAttribute("errorModify", "Le contenu du message est resté le même, merci de le modifier.");
            request.getRequestDispatcher("mymessages").forward(request, response);
            return;
        }


        //Retour vers la page et confirmation
        request.setAttribute("confirmModify", "Le message a bien été modifié !");

        request.getRequestDispatcher("mymessages").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

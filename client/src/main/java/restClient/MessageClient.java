package restClient;

import model.Message;
import model.User;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MessageClient {

    private static final String REST_URI = "http://localhost:8080/server";
    private static Client clientUser = ClientBuilder.newClient();;
    private static WebTarget webTargetUser = clientUser.target(REST_URI);


    public static List<Message> getAllMessages() {
        GenericType<List<Message>> genericType = new GenericType<List<Message>>() {};
        List messages = null;

        Response responseMessages = webTargetUser
                .path("restapp")
                .path("MessagesService")
                .path("messages")
                .request()
                //La resource est fournie en JSON, la conversion en java se fera grâce à GENSON
                .accept(MediaType.APPLICATION_JSON)
                .get();

        int status = responseMessages.getStatus();

        if(status==200) {
            messages = responseMessages.readEntity(genericType);
            return new ArrayList<>(messages);
        }
        //Accès à la ressource fournie par le service en suivant l'url REST_URI
        return new ArrayList<>();

    }

    public static List<Message> getMessagesOfUser(String username) {

        GenericType<List<Message>> genericType = new GenericType<List<Message>>() {};
        List messages = null;

        //Accès à la ressource fournie par le service en suivant l'url REST_URI
        Response responseMessages = webTargetUser
                .path("restapp")
                .path("MessagesService")
                .path(username)
                .path("messages")
                .request()
                //La resource est fournie en JSON, la conversion en java se fera grâce à GENSON
                .accept(MediaType.APPLICATION_JSON)
                .get();

        int status = responseMessages.getStatus();

        if(status==200) {
            messages = responseMessages.readEntity(genericType);
            return new ArrayList<>(messages);
        }
        //Accès à la ressource fournie par le service en suivant l'url REST_URI
        return new ArrayList<>();
    }


    public static Message addMessage(Message messageToAdd) {

        //Accès à la ressource fournie par le service en suivant l'url REST_URI
        Response response = webTargetUser
                .path("restapp")
                .path("MessagesService")
                .path("messages")
                .request()
                //La resource est fournie en JSON, la conversion en java se fera grâce à GENSON
                .accept(MediaType.APPLICATION_JSON)
                .post(Entity.json(messageToAdd));

        Message messageAdded = response.readEntity(Message.class);

        return messageAdded;
    }

    public static Message getMessage(int idMessage) {

        GenericType<Message> genericType = new GenericType<Message>() {};

        //Accès à la ressource fournie par le service en suivant l'url REST_URI
        Response response = webTargetUser
                .path("restapp")
                .path("MessagesService")
                .path("messages")
                .path(String.valueOf(idMessage))
                .request()
                //La resource est fournie en JSON, la conversion en java se fera grâce à GENSON
                .accept(MediaType.APPLICATION_JSON)
                .get();

        Message message = response.readEntity(genericType);

        return message;
    }

    public static Message updateMessage(int idMessage, Message newData) {

        GenericType<Message> genericType = new GenericType<Message>() {};

        //Accès à la ressource fournie par le service en suivant l'url REST_URI
        Response response = webTargetUser
                .path("restapp")
                .path("MessagesService")
                .path("update")
                .path(String.valueOf(idMessage))
                .request()
                //La resource est fournie en JSON, la conversion en java se fera grâce à GENSON
                .accept(MediaType.APPLICATION_JSON)
                .put(Entity.json(newData));

        Message updatedMessage = response.readEntity(genericType);

        return updatedMessage;
    }


    public static void deleteMessage(int idMessage) {

        Response response = webTargetUser
                .path("restapp")
                .path("MessagesService")
                .path("delete")
                .path(String.valueOf(idMessage))
                .request()
                //La resource est fournie en JSON, la conversion en java se fera grâce à GENSON
                .accept(MediaType.APPLICATION_JSON)
                .delete();
    }

}

package restClient;

import model.User;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class UserClient {

    private static final String REST_URI = "http://localhost:8080/server";
    private static Client clientUser = ClientBuilder.newClient();;
    private static WebTarget webTargetUser = clientUser.target(REST_URI);


    public static ArrayList<User> getAllUsers() {

        GenericType<List<User>> genericType = new GenericType<List<User>>() {};
        List<User> users = null;

        //Accès à la ressource fournie par le service en suivant l'url REST_URI
        Response responseUsers = webTargetUser
                .path("restapp")
                .path("UserService")
                .path("users")
                .request()
                //La resource est fournie en JSON, la conversion en java se fera grâce à GSON
                .accept(MediaType.APPLICATION_JSON)
                .get();

        int status = responseUsers.getStatus();

        if(status==200) {
            users = responseUsers.readEntity(genericType);
            return new ArrayList<>(users);
        }
        return new ArrayList<>();


    }

    public static User getUserByCredentials(String username, String password) {

        GenericType<User> genericType = new GenericType<User>() {};
        User user = null;

        //Accès à la ressource fournie par le service en suivant l'url REST_URI
        Response responseUser = webTargetUser
                .path("restapp")
                .path("UserService")
                .path("user")
                .queryParam("username", username)
                .queryParam("password", password)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();

        int status = responseUser.getStatus();

        if(status==200) {
            user = responseUser.readEntity(genericType);
            return user;
        }
        return null;
    }
}
